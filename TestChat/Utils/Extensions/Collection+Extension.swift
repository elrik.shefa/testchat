//
//  Collection+Extension.swift
//  TestChat
//
//  Created by Матвей Чернышев on 12.06.2020.
//  Copyright © 2020 Matvey Chernyshov. All rights reserved.
//

import Foundation

extension Collection {
	
	var isNotEmpty: Bool {
		return !isEmpty
	}
}

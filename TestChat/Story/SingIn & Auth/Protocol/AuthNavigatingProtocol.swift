//
//  AuthNavigatingDelegate.swift
//  TestChat
//
//  Created by Матвей Чернышев on 18.06.2020.
//  Copyright © 2020 Matvey Chernyshov. All rights reserved.
//

protocol AuthNavigatingProtocol: class {
	func toLoginVC()
	
	func toSingUpVC()
}
